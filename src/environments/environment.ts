export const ENV = {
  API_B2B: 'http://service.martins.com.br/b2bservice.asmx',
  API_MRKT: 'https://www.martins.com.br/Marketing.WebService/CampanhaService.asmx',
  API_Backend: 'https://admin.parceiromartins.com.br/api/v1/directlists',
  BackgroundImage: 'assets/imgs/background/background-2.jpg',
  LogoImage: 'assets/imgs/LogoMartins.png'
}
