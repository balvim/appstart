# appStart

Modelo de Aplicativo Móvel para ser utilizado como base para projetos multiplaforma

## Clone o repositório 
```bash
$ git clone https://<user>@bitbucket.org/balvim/appstart.git
```

## Instale e Execute
```bash
$ cd appstart
$ npm install
$ ionic serve 
```

## Adicionar plataformas (Android e iOS)
```bash
$ cd appstart
$ ionic cordova platform add android
$ ionic cordova platform add ios
```

## Build para distribuição (Android)
```bash
$ ionic cordova build --release --prod
```
